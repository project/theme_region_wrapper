### Overview

Theme region wrapper module provides UI to specify HTML element for theme's
each region. You can configure each wrapper element, CSS classes and
WAI-ARIA role on a theme settings page.

#### Motivation

The core views module can change field HTML output with provided UI,
and some contribution modules such as [Fences](https://www.drupal.org/project/fences), [Block Class](https://www.drupal.org/project/block_class) and so on,
also can change HTML output with module provided UI. When you want to change
a theme region's wrapper HTML, however cannot change with UI,
so need to rewrite the `page.html.twig` or `region.html.twig`.

#### NOTE

This module **overrides only a region template which is located in the
core directory**. If a region uses twig template which is located in a
custom theme directory, configured settings for that region does not
override anything.

### Features

Theme's each region can configure following list items from UI
on theme settings page.

- HTML element
- CSS classes
- WAI-ARIA role

### How to configure a region wrapper settings for a theme?

1. Go to `/admin/appearance`.
2. Click a theme "Settings" link in the installed themes list.
3. Click "Theme region wrapper settings" header.
4. Configure each region wrapper output.
5. Click "Save configuration" button.

### Override HTML element settings (advanced)

If you want not to use default HTML element settings,
you can override settings.
You want to override the configuration by other module installation, see `hook_install()` code at this module's [testing code](https://git.drupalcode.org/project/theme_region_wrapper/-/blob/1.0.x/tests/modules/theme_region_wrapper_element_override_test/theme_region_wrapper_element_override_test.install?ref_type=heads#L11-16).
