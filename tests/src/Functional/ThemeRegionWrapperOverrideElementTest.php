<?php

namespace Drupal\Tests\theme_region_wrapper\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group theme_region_wrapper
 */
class ThemeRegionWrapperOverrideElementTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'theme_region_wrapper_element_override_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);
  }

  /**
   * Tests overriding element settings.
   */
  public function testThemeRegionWrapperOverrideElementSettings():void {
    $this->drupalGet('/admin/appearance/settings/stark');

    $session = $this->assertSession();
    $session->optionExists('region_wrapper[breadcrumb][form][element]', 'template');
    $session->optionNotExists('region_wrapper[breadcrumb][form][element]', 'section');
  }

}
