<?php

namespace Drupal\Tests\theme_region_wrapper\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the UI and region HTML output.
 *
 * @group theme_region_wrapper
 */
class ThemeRegionWrapperOutputTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'theme_region_wrapper',
    'block',
    'block_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp():void {
    parent::setUp();
    $this->drupalLogin($this->rootUser);

    $this->drupalPlaceBlock('test_html', ['id' => 'test_html_block']);

    // Enable a menu block, to test more complicated HTML.
    $this->drupalPlaceBlock('system_menu_block:admin', ['id' => 'test_menu_block']);
  }

  /**
   * Tests settings form output on stark theme settings page.
   */
  public function testThemeRegionWrapperStarkSettingsForm():void {
    $this->drupalGet('/admin/appearance/settings/stark');

    $session = $this->assertSession();
    $session->elementAttributeNotExists('css', '#edit-region-wrapper', 'open');
    $session->elementTextEquals('css', '#region_wrapper_form', 'Theme region wrapper settings');

    $session->elementsCount('css', '#edit-region-wrapper tr[data-drupal-selector^="edit-region-wrapper-"]', 10);

    $session->fieldValueEquals('region_wrapper[breadcrumb][form][element]', 'div');
    $session->fieldValueEquals('region_wrapper[breadcrumb][form][classes]', '');
    $session->fieldValueEquals('region_wrapper[breadcrumb][form][aria_role]', '');

    $session->selectExists('region_wrapper[breadcrumb][form][element]');
  }

  /**
   * Tests HTML output.
   */
  public function testStarkRegionChangingOutput():void {
    $this->drupalGet('/admin/appearance/settings/stark');
    $page = $this->getSession()->getPage();

    // Set the content region output.
    $page->selectFieldOption('region_wrapper[content][form][element]', 'section');
    $page->fillField('region_wrapper[content][form][classes]', 'content-test changing-classes');
    $page->fillField('region_wrapper[content][form][aria_role]', 'presentation');

    $page->pressButton('Save configuration');

    $session = $this->assertSession();

    $session->fieldValueEquals('region_wrapper[content][form][element]', 'section');
    $session->fieldValueEquals('region_wrapper[content][form][classes]', 'content-test changing-classes');
    $session->fieldValueEquals('region_wrapper[content][form][aria_role]', 'presentation');

    $session->elementExists('css', 'section.content-test.changing-classes[role=presentation]');

    // No element wrapper test.
    $page = $this->getSession()->getPage();
    $page->selectFieldOption('region_wrapper[content][form][element]', 'none');

    $page->pressButton('Save configuration');

    $session = $this->assertSession();
    $session->elementNotExists('css', '.content-test.changing-classes[role=presentation]');
  }

  /**
   * Tests hook_preprocess_region() output.
   */
  public function testRegionHookOutput():void {
    $this->container->get('module_installer')->install(['theme_region_wrapper_region_hook_test']);
    $this->drupalGet('/admin/appearance/settings/stark');
    $page = $this->getSession()->getPage();

    // Set the content region output.
    $page->fillField('region_wrapper[content][form][classes]', 'content-test changing-classes');

    $page->pressButton('Save configuration');

    $session = $this->assertSession();

    $session->fieldValueEquals('region_wrapper[content][form][classes]', 'content-test changing-classes');

    $session->elementExists('css', 'div.new_class.content-test.changing-classes');
  }

}
